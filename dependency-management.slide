# A History of Dependency Management
1.0 to Workspaces

John Anthony
Senior Cloud Software Engineer, Humn
17 Aug 2022

https://humn.ai
john.anthony@humn.ai

https://jo.hnanthony.com
john@jo.hnanthony.com

https://gitlab.com/JohnAnthony/talks

## About me
* Lifelong developer, open-source contributor
* Using Go for serious business since March 2012, v1.0
* Real practical Go user, looking to share real history
* First contribution to Go at start of this year, a day 1 bugfix for workspaces
* Say hello! Things to talk to me about if you see me at the con!
  * Digital rights, effective altruism
  * Electronics/maker movement
  * "New wave" 60s/70s science fiction books
  * Old-school roleplaying games
  * Single malt whiskey
  * Veganism & environmental sustainability

##
.image img/Humn_logo_pos.png _ 500

&nbsp;

* We do smart things with fleet insurance
* If I do something you like then thank my company, if I do something you don't like it's on me
* Lots of people contributed ideas and helped refine this talk
* Gold sponsor of Gophercon UK 2022
* Robbie King - "The Humn Side of GraphQL" tomorrow @ 11:15
* We have a booth, come and see us!
* We're hiring!

## Purposes
* Talk about external package dependencies as related to use in Go
* Useful for both veterans (enjoy the nostalgia) and newbies (good overview)
* Broad overview of 10+ years of Go
* Some context (and drama) from outside of Go
* Personal viewpoint, but treated as an academic exercise
* Lots of information about this out there, but not organised and cohesive
* Repeated and improved - contact information for even small corrections
* Time at the end for questions, comments, discussion
* Contact information on last slide, will leave up on screen

## This talk is NOT
* A full history of dependency management
* About dependency injection
* About runtime dependencies, static vs. dynamic linking, cgo
* Intended to change how you work (unless you're not yet using modules)
* Not just about modules and how to use them
* About graphs, topological sorting, other computer science aspects of resolving dependencies

.image img/nottop.png 220 _

## Timeline
.image img/timeline.png _ 950
.caption Time in standard universes monotonically increases (from left to right in this illustration) - ISO 8601, probably

## Quick demographics

&nbsp;

<p style="text-align: center;"><b>Never</b> used Go beyond toy projects/learning</p>

&nbsp;

<p style="text-align: center;">Used Go only <b>with</b> modules</p>

&nbsp;

<p style="text-align: center;">Used Go only <b>without</b> modules (GOROOT)</p>

&nbsp;

<p style="text-align: center;">Used Go <b>both</b> with and without modules</p>

## Pre-1.0
* Go famously dreamed up during long compile times for C++ (Griesemer, Pike, Thompson)
* [Go at Google: Language Design in the Service of Software Engineering](https://go.dev/talks/2012/splash.article), Rob Pike, 2012
  * Section 5 - Dependencies in C and C++
  * Section 7 - Dependencies in Go
* Single easy read of object file for each dependency
* No circular dependencies
* Decentralised (external dependencies represented by URLs)

&nbsp;

*"This approach to dependency management is the single biggest reason why Go compilations are faster than C or C++ compilations"*

## Go 1 and the Go 1 Compatibility Document
* Program written for v1.0 will compile, may even be very good by modern standards
* Some best practises have changed
* Largely orthogonal to dependency management but worth mentioning
* No compatibility promise for tooling but generally quite stable

&nbsp;

		$ go get gitlab.com/JohnAnthony/simplesrv
		go: go.mod file not found in current directory or any parent directory.
			'go get' is no longer supported outside a module.
			To build and install a command, use 'go install' with a version,
			like 'go install example.com/cmd@latest'
			For more information, see https://golang.org/doc/go-get-install-deprecation
			or run 'go help get' or 'go help install'.

## The Dark Ages: 1.0 to 1.10
* [How to Write Go Code (with GOPATH)](https://go.dev/doc/gopath_code)

* env `GOPATH`, containing dependencies
  * Download dependencies to `GOPATH` with "`go get github.com/foo/bar`"
  * Since Go 1.8, defaults to `$HOME/go`
  * `src/` and `bin/` with version control in each project within `src/`

## 

.image img/layout.png 600 _

## Problems

* Working with lots of code from different projects not great
  * Change `GOPATH` every time
  * Multiple unrelated projects in the same workspace
* Doesn't mesh with how pretty much everyone else does dependencies
* Lots of people having the same problems, asking the same questions
* "It should just work like npm, npm is perfect and the next slide isn't going to ruin that"

## Drama
.image img/npm.png _ 950

## 
.image img/xkcd-2347.png 500 _
.caption xkcd #2347

## 
* Russ Cox is a busy bee
	* [Our Software Dependency Problem](https://research.swtch.com/deps)
	* [Surviving Software Dependencies](https://research.google/pubs/pub50201/)
* He's right
* Maliciously altered dependencies are also a problem
* Go doesn't solve this problem, but there are things which can mitigate it
  * Don't use unnecessary dependencies
  * Pick dependencies with few dependencies themselves
  * Use dependencies published by people you trust
  * Pinned versions and checksums
  * Use vendoring

## Third party tools
* Everybody's writing tools for dependency management
* `depman`, `gopm`, `gpm`, `goop`, `godep`, `gopkg`, `party`, `vendorize`, ...
* Interesting to study
* Don't actually use them, they're are all long dead and deprecated

## Timeline again
.image img/timeline2.png _ 950

## dep
* An "official experiment to implement a package manager for Go"
* Maintained by Sam Boyer
* Github 13k stars, 1.1k forks
* Familiar file layout
  * `Gopkg.toml`
  * `Gopkg.lock`
  * `vendor/`
* Recognisable usage
  * `dep init`
  * `dep ensure -add github.com/foo/bar`
* Deprecated in 2020 in favour of...

## Modules! aka "first-class package versioning"
* [https://go.dev/doc/modules/managing-dependencies](https://go.dev/doc/modules/managing-dependencies)
* Pre-release as `vgo` in Go 1.10 (Feb 2018)
* Initial release in Go 1.11 (Aug 2018)
* Finalised in Go 1.13 (Aug 2019)
* `GO111MODULE` environment variable
  * Needed to be set to `on` until Go 1.16 (Feb 2021)
  * Now on by default
  * Can be set as `GO111MODULE="off"` to use legacy behaviour

## 

* No more `GOPATH`
* Visible influences from `dep` file layout
  * `Gopkg.toml` => `go.mod`
  * `Gopkg.lock` => `go.sum`
  * `vendor/` is the same
* Usage
  * `go mod init gitlab.com/JohnAnthony/rot13unbreakablesecurity`
  * `go get gitlab.com/foo/bar`

## go.mod

&nbsp;

		module github.com/foo/bar

		go 1.19

		require (
			github.com/baz/qux v1.8.0
			go.uber.org/zap v1.17.0
		)

		require (
			github.com/davecgh/go-spew/spew v1.1.1 // indirect
            ... // Lots more here removed for brevity
		)

		replace github.com/baz/qux => /home/ja/projects/qux

		exclude go.uber.org/zap v1.17

		retract v0.2.0

Keywords: `module`, `go`, `require`, `replace`, `exclude`, `retract`

## Maintaining modules
* `go get`

		$ go build .
		main.go:4:2: no required module provides package github.com/davecgh/go-spew/spew; to add it:
			go get github.com/davecgh/go-spew/spew

* `go get -u ./...`

		go: downloading go.uber.org/zap v1.22.0
		go: upgraded go.uber.org/zap v1.17.0 => v1.22.0

* `go mod tidy`

## Module security (go.sum)

&nbsp;

		$ cat go.sum
		github.com/davecgh/go-spew v1.1.1 h1:vj9j/u1bqnvCEfJOwUhtlOARqs3+rkHYY13jYWTU97c=
		github.com/davecgh/go-spew v1.1.1/go.mod h1:J7Y8YcW2NihsgmVo/mv3lAwl/skON4iLHjSsI+c5H38=
&nbsp;

* [Module Mirror and Checksum Database Launched](https://go.dev/blog/module-mirror-launch)
* Module mirror is used to pull modules (default: `proxy.golang.org`)
  * Makes things faster by speaking a protocol other than, say, git over HTTP
* Hashes are pulled from a checksum database (default: `sum.golang.org`)
  * Downloaded packages are verified against those hashes
* Module index available at `index.golang.org`

## Proxies and privacy
* Some drama
* Using private repositories (srs business)
  * `GOPRIVATE=github.com/my-company`
* Disable proxy and/or sumdb with speed and security implications
  * `GOPROXY=direct`
  * `GOSUMDB=off`
* Alternatives, like `goproxy.io` + `gosum.io`
  * `GOPROXY=https://goproxy.io`
  * `GOSUMDB=https://gosum.io`

## Vendoring
* Fancy name for "just copy all your dependencies in with your project"
* That npm unpublish problem? Not a problem any more
* "A little copying is better than a little dependency"
* Go back and read those articles by Russ Cox
* Since Go 1.14 (Feb 2020) if `vendor/modules.txt` is present, vendored dependencies resolve locally automagically
  * Prior to Go 1.14, the `-mod=vendor` flag was required

&nbsp;

		$ cat go.mod
		module github.com/qux/bar

		go 1.19

		require github.com/davecgh/go-spew v1.1.1

## 

		$ go mod vendor

		$ tree vendor/
		vendor
		├── github.com
		│   └── davecgh
		│       └── go-spew
		│           ├── LICENSE
		│           └── spew
		│               ├── bypass.go
		│               ├── bypasssafe.go
		│               ├── common.go
		│               ├── config.go
		│               ├── doc.go
		│               ├── dump.go
		│               ├── format.go
		│               └── spew.go
		└── modules.txt

		4 directories, 10 files

## Workspaces
* Released in Go 1.18 (Feb 2022)
* [Get Familiar with Workspaces](https://go.dev/blog/get-familiar-with-workspaces)
* Workspaces are back, but they are now **collections of modules** that resolve locally
* Similar to `go.mod` keyword `replace`
  * Doesn't change any version controlled files
  * Allows easy ad-hock changes to your working environment


## 

* `go work init <folders>` to create a `go.work` file

		$ go work init foo/ bar/
		$ cat go.work
		go 1.19

		use (
			./foo
			./bar
		)

* `go work use <folders>` to add more entries to `go.work` file

		$ go work use baz/
		$ cat go.work
		go 1.19

		use (
			./foo
			./bar
			./baz
		)

* Just delete `go.work` if you don't need your workspace anymore

## Best practises in 2022

**Do**
* Use `go mod` modules

**Maybe**
* Use `go mod vendor` vendoring
* Use `go work` workspaces

**Don't**
* Use `GOPATH`-style workspaces

## The future: vulnerability management
* [Go Vulnerability Management](https://go.dev/security/vulndb/)
* `go mod audit` or some similar command
* `vuln.go.dev` already has listings of known vulnerabilities
* You can try it now

&nbsp;

		$ go install golang.org/x/vuln/cmd/govulncheck@latest
		$ govulncheck ./...
			govulncheck is an experimental tool. Share feedback at https://go.dev/s/govulncheck-feedback.

			Scanning for dependencies with known vulnerabilities...

## Wrap-up
.image img/timeline.png _ 950

##

&nbsp;

&nbsp;

.image img/Humn_logo_pos.png _ 950
.caption DID I MENTION WE'RE HIRING?
