#!/bin/env bash

if ! command -v present &> /dev/null
then
    echo "present could not be found"
	echo "  maybe run \`go install golang.org/x/tools/cmd/present@latest\`"
    exit 1
fi

present -notes -base ./theme .
